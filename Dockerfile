FROM alpine:latest as build-stage
WORKDIR /app
RUN apk update && apk upgrade && \
    apk add --no-cache bash openssh openssl wget unzip
RUN wget https://gitlab.com/Manuel_Raven/thw-adressbuch/-/jobs/artifacts/master/download?job=build-pwa  -O artifacts.zip
RUN unzip artifacts.zip
FROM socialengine/nginx-spa:latest as production-stage
COPY --from=build-stage /app/dist/pwa /app
RUN chmod -R 777 /app
EXPOSE 80
ENV PORT 80